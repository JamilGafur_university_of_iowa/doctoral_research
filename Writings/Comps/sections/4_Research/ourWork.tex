\section{Open Problems in Enhancer Prediction and the Need for Explainable Machine Learning}
\label{sec:openproblem}


As we delve deeper into the realm of chromatin enhancer prediction through machine learning (ML) techniques, it becomes evident that while these approaches have yielded impressive results, they are not without their challenges and limitations. One of the pressing concerns in this field revolves around the "black-box" nature of many ML models, which raises questions about what exactly these models have learned and how they arrive at their predictions. This lack of transparency underscores the need for explainable machine learning (XAI) to elucidate the biological factors governing enhancer functionality and to ensure the reliability of predictive models.

Machine learning has undeniably played a pivotal role in advancing enhancer prediction. Models like DeepEnhancer \cite{min2017predicting} and iEnhancer-EBLSTM \cite{niu2021ienhancer}, as discussed in previous sections, have showcased the potential of ML in discerning complex patterns within DNA sequences. These models can identify enhancers and estimate their strengths, offering valuable insights into gene regulation and potential therapeutic targets. However, their success often comes at the cost of interpretability.

The inherent complexity of ML models, especially deep neural networks, poses a significant challenge. These models operate by learning intricate relationships between input features, but they do so in a manner that is often inscrutable to human understanding. This opacity can be problematic in genomics, where biological insights are highly valued.

To address this issue, explainable machine learning techniques are gaining traction. XAI methods aim to provide clear and interpretable explanations for the decisions made by ML models. In the context of enhancer prediction, XAI can help researchers understand which sequence motifs, epigenetic marks, or other genomic features are influential in the model's predictions. This insight is crucial for validating the model's biological relevance and for identifying potential regulatory elements that were previously unknown.

One of the central challenges in enhancer prediction is discerning the subtle and context-dependent factors that govern enhancer functionality. Enhancers can exhibit tissue-specific activity and respond to various environmental cues, making their prediction a complex task. ML models excel at identifying patterns in large and high-dimensional genomic datasets, but they might not provide a clear understanding of why a particular sequence is deemed an enhancer or how it functions within a specific biological context.

Explainability can bridge this gap by offering insights into the mechanistic basis of enhancer predictions. By visualizing which regions of DNA or epigenetic marks contribute most significantly to enhancer predictions, researchers can formulate hypotheses about the regulatory elements involved. This not only enhances our understanding of enhancer biology but also provides a means of validating the model's predictions in a biologically meaningful way.

Moreover, explainable machine learning can aid in addressing another critical issue in enhancer prediction: model robustness and generalizability. ML models, if not properly validated, can overfit to the training data and perform poorly on unseen data. XAI can help identify whether a model relies on spurious correlations or genuinely informative features, ensuring that the model's predictions are reliable and can be applied to diverse genomic contexts.


\section{Current work for model interpretability}

Enhancer prediction models, like many other machine learning models, have the advantage of making accurate predictions. However, their complex decision-making processes often remain shrouded in mystery, raising concerns about their reliability and biological relevance. In response to these challenges, the field of eXplainable AI (XAI) has introduced valuable tools to improve model interpretability. One such tool is SHAP (SHapley Additive exPlanations), a versatile technique rooted in cooperative game theory. SHAP has the potential to illuminate the inner workings of enhancer prediction models, shedding light on which genomic features drive their predictions. In this section, we explore the principles of SHAP and its applications in enhancer prediction, while also acknowledging the limitations and challenges that come with its adoption in the field of bioinformatics.


\subsection{SHAP}
\label{sec:SHAP}

In the pursuit of enhancing the transparency and interpretability of complex models like enhancer predictors, eXplainable AI (XAI) methods have emerged as a valuable toolkit. Among these methods, SHAP (SHapley Additive exPlanations) stands out as a versatile technique that holds great potential for shedding light on the decision-making processes of models, such as those employed in enhancer prediction.

SHAP is rooted in the concept of Shapley values from cooperative game theory, offering a principled way to distribute contributions among a set of players. In the context of machine learning models, SHAP quantifies the contribution of each input feature to a specific prediction. This answers the question: "How much does each feature influence the model's prediction for this particular instance?"

One key advantage of SHAP lies in its capacity to provide a unified framework for interpreting various types of models, ranging from traditional linear models to intricate deep neural networks. In the case of enhancer prediction, where input data consists of genomic sequences with complex patterns, SHAP's versatility is particularly valuable. By applying SHAP to the predictions of an enhancer predictor, researchers can gain insights into which specific nucleotide positions or motifs exert the most influence on the model's decisions.

The process of generating SHAP values involves evaluating the model's prediction for a given instance while systematically including and excluding different features. By observing how the prediction changes as specific features are included or excluded, SHAP values are computed using the following equation:

\begin{equation}
    \text{SHAP}(x_i) = \sum_{S \subseteq N \setminus \{i\}} \frac{|S|!(|N|-|S|-1)!}{|N|!} [f(x_{S \cup \{i\}}) - f(x_S)]
\end{equation}

Where:
\begin{enumerate}
    \item \(x_i\) is the instance with feature \(i\) set.
    \item \(N\) is the set of all features.
    \item \(S\) is a subset of features excluding \(i\).
    \item \(f(x_S)\) is the model's prediction with features in \(S\) set.
    \item \(f(x_{S \cup \{i\}})\) is the model's prediction with both \(S\) and \(i\) features set.
\end{enumerate}

These values assign contributions to each feature, revealing their impact on the prediction. For enhancer prediction models, this means understanding which sequence positions are pivotal in determining whether a sequence is classified as an enhancer or not.

To illustrate the power of SHAP in enhancer prediction, consider the scenario where the model identifies a particular sequence as an enhancer. Applying SHAP can uncover the sequence positions that significantly contribute to this positive prediction. This information can then be compared against known regulatory motifs associated with enhancers, allowing researchers to verify whether the model's decision aligns with established biological knowledge.

However, while SHAP offers a promising approach to model interpretability, it is not without challenges. Applying SHAP to complex models, such as those used in bioinformatics, demands careful consideration of computational resources and the intricacies of genomic data. Moreover, ensuring that the generated SHAP values are both accurate and meaningful necessitates rigorous validation against established biological motifs and patterns.

\subsection{Limitations of SHAP in Bioinformatics}

One notable downside of SHAP is its computational complexity. Calculating SHAP values involves evaluating the model's prediction for multiple combinations of features, which can become computationally expensive, particularly for models with a large number of input features. This complexity can hinder the application of SHAP to enhancer prediction models, especially when dealing with extensive genomic sequences.

Additionally, SHAP values might not always provide intuitive insights into the decision-making process of the model. While SHAP quantifies the contribution of each feature to a specific prediction, it doesn't necessarily explain why the model assigns a particular weight to that feature. This lack of causative insight can limit the depth of understanding gained from SHAP explanations, potentially leaving researchers with statistical associations without clear biological context.

Furthermore, SHAP values might not be universally applicable to all types of data and models. Certain scenarios, such as interactions between features or non-linear relationships, could lead to challenges in accurately interpreting SHAP values. Ensuring that SHAP explanations are both meaningful and informative requires careful consideration of the underlying data and model characteristics.

Validation of SHAP explanations is also a concern, particularly in bioinformatics applications. While SHAP values quantify feature contributions, it's crucial to verify that these contributions align with established biological knowledge. Ensuring that SHAP-derived insights correspond to known regulatory motifs and patterns demands rigorous validation against domain-specific databases and experimental findings.