\documentclass{article}

\begin{document}
\section{Model Agnostic Methods}

Model-agnostic methods, do not rely on the specifics of a particular model's architecture. They are designed to provide explanations and insights that can be applied to any machine learning model, regardless of its complexity or type. These methods are valuable when the internal workings of the model are not easily accessible or when explanations need to be model-agnostic for broader applicability.

\subsection{Instance-wise Feature Selection}

Instance-wise feature selection is a model-agnostic method that focuses on understanding the importance of individual features for a specific data instance. It includes the following sub-components:

\begin{enumerate}
    \item \textbf{Local Feature Importance:} This analysis assesses how changes in the values of features within the neighborhood of a particular data point affect the model's output for that specific instance. It helps identify which features are most influential in a local context.
    
    \item \textbf{Global Feature Importance:} Global feature importance, on the other hand, examines the impact of feature variations across the entire dataset on the model's predictions. It provides a broader perspective on feature importance that applies globally.
    
    \item \textbf{Direct Model Interpretation:} This aspect of instance-wise feature selection aims to provide a comprehensive understanding of the model's behavior in the context of individual data instances.  This usually includes models that are inherently interpretable, such as decision trees or linear models.
    \item 
\end{enumerate}

These methods are particularly useful when one needs to gain insights into how a model makes predictions for specific data points and which features play a crucial role in those predictions. They can help identify both local and global patterns in the model's decision-making process.



\subsection{Local Methods}

Local methods focus on interpreting the model's behavior on a local scale, understanding how it reacts to specific input perturbations.

\subsubsection{Gradient (grad)}

The gradient of the model's output with respect to the input features provides insights into how the model responds to changes in input data:

\[
\nabla f(X) = \left(\frac{\partial f}{\partial x_1}, \frac{\partial f}{\partial x_2}, \ldots, \frac{\partial f}{\partial x_n}\right)
\]

Where $\nabla f(X)$ is the gradient vector and $x_i$ are input features.

\subsubsection{Guided Backpropagation (GBP)}

Guided Backpropagation (GBP) is an improvement over standard backpropagation for visualizing which parts of the input contribute to the model's predictions. It enhances the interpretability of the gradient information.

\subsection{Sampling Methods}

Sampling methods approximate complex model behaviors by analyzing subsets of the data.

\subsubsection{LIME (Local Interpretable Model-Agnostic Explanations)}

LIME generates locally faithful explanations by fitting interpretable models to perturbed versions of the input data.

\subsubsection{SHAP (SHapley Additive exPlanations)}

SHAP values provide a unified framework for feature attribution in a way that fairly distributes feature importance among the contributing features.

\subsubsection{CXPlain}

CXPlain combines counterfactual explanations with data augmentation to provide model-agnostic interpretability.

\subsection{Hybrid Methods}

Hybrid methods combine different interpretability techniques to gain a more comprehensive understanding of model behavior.

\subsubsection{SmoothGrad}

SmoothGrad improves the interpretability of gradient-based methods by smoothing the perturbations applied to the input data.

\subsubsection{Vargrad}

Vargrad enhances gradient-based explanations by considering the variance in gradients over multiple input perturbations.

\subsubsection{INFD (Influence-based Feature Distillation)}

INFD distills feature importance information from an ensemble of interpretable models, providing a more robust explanation of model predictions.

\subsection{Global Methods}

Global methods focus on understanding the model's behavior across the entire dataset.

\subsubsection{DeepLIFT (Deep Learning Important FeaTures)}

DeepLIFT assigns relevance scores to each feature based on its contribution to the model's output, considering both the forward and backward propagation of information.

\subsubsection{Integrated Gradients}

Integrated Gradients compute feature attributions by integrating the model's gradients with respect to input data along a path from a reference point to the input.

\subsubsection{LRP (Layer-wise Relevance Propagation)}

LRP assigns relevance scores to input features based on their impact on the model's output and is particularly useful for deep neural networks.


\end{document}