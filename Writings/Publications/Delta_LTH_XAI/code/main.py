import os
import torch
import pandas as pd
import numpy as np
from lth_xai.modelBuilder import modelBuilder
from lth_xai.dataset import datasetBuilder
from lth_xai.trainer import train
from lth_xai.pruner import modelApplyMask
from lth_xai.modelSaver import save_model
import gc
from typing import Union, List
import argparse
import h5py as hdf5
import matplotlib.pyplot as plt


def plot_metrics(data, dataset_name, model_name):
    original = data["original"]
    for pruning_type, pruning_data in data.items():
        if pruning_type == "original":
            continue
        for pruning_data_type, pruning_data_data in pruning_data.items():
            for pruning_target, pruning_target_data in pruning_data_data.items():
                fig, ax1 = plt.subplots()
                ax2 = ax1.twinx()  # Create a twin Axes sharing the x-axis

                training_accuracy = original["train_accuracy"]
                validation_accuracy = original["val_accuracy"]
                training_loss = original["train_loss"]
                validation_loss = original["val_loss"]
                vlines = [len(training_accuracy)-1]

                for study, study_data in pruning_target_data.items():
                    training_accuracy = np.concatenate((training_accuracy, study_data["train_accuracy"]))
                    validation_accuracy = np.concatenate((validation_accuracy, study_data["val_accuracy"]))
                    training_loss = np.concatenate((training_loss, study_data["train_loss"]))
                    validation_loss = np.concatenate((validation_loss, study_data["val_loss"]))

                    vlines.append(len(training_accuracy)-1)


                ax1.plot(training_accuracy, label="training accuracy", color="orange")
                ax1.plot(validation_accuracy, label="validation accuracy", color="gold")
                ax2.plot(training_loss, label="training loss", color="royalblue", linestyle="dashed")
                ax2.plot(validation_loss, label="validation loss", color='crimson', linestyle="dashed")

                for vline in vlines:
                    ax1.axvline(vline, color="black", linestyle="dotted")

                # legends outside the left plot
                ax1.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                ax2.legend(bbox_to_anchor=(1.05, 0.9), loc='upper left')
                # labels
                ax1.set_xlabel('Epoch')
                ax1.set_ylabel('Accuracy', color='g')
                ax2.set_ylabel('Loss', color='b')

                plt.title(f"{pruning_type} {pruning_data_type} {pruning_target}")

                os.makedirs(f"./plots/{model_name}_{dataset_name}", exist_ok=True)
                plt.tight_layout()
                plt.savefig(f"./plots/{model_name}_{dataset_name}/{pruning_type}_{pruning_data_type}_{pruning_target}.png")
                plt.savefig(f"./plots/{model_name}_{dataset_name}/{pruning_type}_{pruning_data_type}_{pruning_target}.svg")
                plt.close()
                print(f"Saved plot for {pruning_type}_{pruning_data_type}_{pruning_target}.png")

    savepickle(data, f"./plots/{model_name}_{dataset_name}/data.pkl")        

def savepickle(data, filename):
    import pickle
    with open(filename, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def setSeeds(args):
    """
    Set seeds for reproducibility.

    Args:
        args (argparse.Namespace): Arguments for the experiment.

    Returns:
        None
    """
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)

def cleanUp():
    """
    Clean up the memory.

    Returns:
        None
    """
    torch.cuda.empty_cache()
    gc.collect()
    

def getArgs():
    parser = argparse.ArgumentParser(description="Run experiments with various models, datasets, and pruning configurations.")
    parser.add_argument("--batch_size", type=int, default=64, help="The batch size for training.")
    parser.add_argument("--num_workers", type=int, default=1, help="The number of data loader workers.")
    parser.add_argument("--train_split", type=float, default=0.5, help="The percentage of the data used for training.")
    parser.add_argument("--val_split", type=float, default=0.4, help="The percentage of the data used for validation.")
    parser.add_argument("--test_split", type=float, default=0.1, help="The percentage of the data used for testing.")
    parser.add_argument("--seed", type=int, default=42, help="The random seed for reproducibility.")
    parser.add_argument("--epochs", type=int, default=10, help="The number of epochs for training.")
    parser.add_argument("--finetune", type=int, default=5, help="The number of epochs for fine-tuning pruned models.")
    parser.add_argument("--save_dir", type=str, default="./simulation", help="The directory to save the results.")
    # list of models
    parser.add_argument("--model_names", nargs="+", default=["RESNET50", "VGG19", "GOOGLENET"], help="The model names.")
    parser.add_argument("--dataset_name", nargs="+", default=["cifar10","tinyimagenet","cifar100"], help="The dataset name.")
    # list of pruning types
    parser.add_argument("--pruning_types", nargs="+", default=["random", "gradientMagnitude", "magnitude"], help="The pruning types.")
    parser.add_argument("--pruning_targets", nargs="+", default=[0.2,  0.9], help="The pruning targets.")
    parser.add_argument("--prune_steps", type=int, default=5, help="The number of pruning steps.")
    # pruning type conservative, liberal, normal
    parser.add_argument("--pruning_type", type=str, default="normal", help="The pruning type. (conservative, liberal, normal)")
    return parser.parse_args()

def main() -> None:
    """
    Main function to run experiments with various configurations.

    Returns:
        None
    """
   
    args = getArgs()
    for arg in vars(args):
        print(f"{arg}: {getattr(args, arg)}")
    print("\n"*5)
    setSeeds(args)


    # for every model, train the head model and perform pruning experiments
    for dataset_name in args.dataset_name:
        for modelName in args.model_names:
            cleanUp()
            print("==="*10)
            print(f"Running experiments with model {modelName} and dataset {dataset_name}.")
            print("==="*10)
            # build the model
            model = modelBuilder(modelName=modelName, 
                                datasetName=dataset_name
                                )
            
            print(f"Running experiments with model {modelName} and dataset {dataset_name}.")
            if torch.cuda.is_available() == False:
                print("Cuda is not being used")
                quit()
            # print the model
            # print(model)
            # print("==="*10)
            data = {}

            # build the dataset
            tr, va, te = datasetBuilder(datasetName=dataset_name, 
                                        batchSize=args.batch_size,
                                        trainSize=args.train_split, 
                                        valSize=args.val_split, 
                                        testSize=args.test_split, 
                                        numWorkers=args.num_workers, 
                                        seed=args.seed
                                        )
            
            # train the model
            optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
            criterion = torch.nn.CrossEntropyLoss()
            model, train_accuracy, test_accuracy, val_accuracy, train_loss, test_loss, val_loss = train(model, tr, va, te, args.epochs,optimizer, criterion, device=torch.device("cuda"))
            standard_weights = model.state_dict()
            data["original"] = {"train_accuracy": train_accuracy, "test_accuracy": test_accuracy, "val_accuracy": val_accuracy, "train_loss": train_loss, "test_loss": test_loss, "val_loss": val_loss}
            # save the model
            os.makedirs(args.save_dir, exist_ok=True)
            save_model(model, args.save_dir, prefix=f"model_{modelName}_{args.dataset_name}", train_accuracy=train_accuracy, test_accuracy=test_accuracy, val_accuracy=val_accuracy, train_loss=train_loss, val_loss=val_loss, test_loss=test_loss)
            print("==="*10)
            print(f"Model {modelName} trained with accuracy: {test_accuracy[-1]}%, validation accuracy: {val_accuracy[-1]}%")
            print("==="*10)
        
            # for every pruning type, prune the model and fine-tune
            for pruning_target in args.pruning_targets:
                for pruning_type in args.pruning_types:
                    liberal_pruning_rate = (np.logspace(np.log10(.001), np.log10(pruning_target), args.prune_steps, endpoint=True),  "logerithmic")
                    normal_pruning_rate = (np.linspace(.001, pruning_target, args.prune_steps,endpoint=True), "linear")
                    conservative_pruning_rate = (np.linspace(.001, pruning_target, args.prune_steps, endpoint=True)**2, "exponential")
                    for pruning_drop in [conservative_pruning_rate, liberal_pruning_rate, normal_pruning_rate]:
                        cleanUp()
                        steps, steps_type = pruning_drop
                        os.makedirs(os.path.join(args.save_dir, f"{pruning_type}_{modelName}_{steps_type}_{pruning_target}"), exist_ok=True)
                        for step in steps:
                            # prune the model
                            # print out all information for the pruning
                            print("==="*10)
                            print(f"\n\nPruning model with type {pruning_type} and {step:.4f} sparsity. {steps_type} {pruning_target} {pruning_drop}")
                            print("==="*10)
                            model = modelApplyMask(model, pruning_rate=step, pruning_type=pruning_type, training_data=tr, device=torch.device("cpu"))
                            # fine-tune the model
                            optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
                            criterion = torch.nn.CrossEntropyLoss()
                            model, train_accuracy, test_accuracy, val_accuracy, train_loss, test_loss, val_loss = train(model, tr, va, te, args.finetune,optimizer, criterion, device=torch.device("cuda"))
                            # if the data does not exist, create it
                            data.setdefault(f"{pruning_type}", {}).setdefault(f"{steps_type}", {}).setdefault(f"{pruning_target}", {})[f"{step}"] = {
                                "train_accuracy": train_accuracy,
                                "test_accuracy": test_accuracy,
                                "val_accuracy": val_accuracy,
                                "train_loss": train_loss,
                                "test_loss": test_loss,
                                "val_loss": val_loss
                            }
                            # save the model
                            save_model(model, os.path.join(args.save_dir, f"{modelName}_{dataset_name}_{pruning_type}_{modelName}_{steps_type}_{pruning_target}"), prefix=f"model_{modelName}_{args.dataset_name}_{step}", train_accuracy=train_accuracy, test_accuracy=test_accuracy, val_accuracy=val_accuracy, train_loss=train_loss, val_loss=val_loss, test_loss=test_loss)
                            
                            # reset the model
                            model = modelBuilder(modelName=modelName, 
                                                datasetName=dataset_name
                                                )
                            model.load_state_dict(standard_weights)
                            plot_metrics(data, dataset_name, modelName)
                            cleanUp()
                                             
if __name__ == "__main__":
    main()