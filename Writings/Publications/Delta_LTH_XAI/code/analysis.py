import os 
import glob
import pandas as pd
import matplotlib.pyplot as plt
from lth_xai import dataset
from lth_xai import modelBuilder
import torch 
import captum.attr as captum
import tqdm
import numpy as np
torch.manual_seed(0)
np.random.seed(0)


def getFiles():
    """
    Get the files for the analysis

    simulations: list of all the simulations
        example: ['simulation/RESNET50_0.2_gradient', 'simulation/RESNET50_0.2_random', 'simulation/RESNET50_0.2_magnitude']
    original_CSV: the original CSV file
        example: 'simulation/model_RESNET%)_cifar_data.csv'
    original_weights: the original weights
        example: 'simulation/model_RESNET50_cifar10.pth'
    prune_weights: list of all the pruned weights
        example: ['simulation/RESNET50_0.2_gradient/model_RESNET50_cifar10_0...pth']
    prune_CSV: list of all the pruned CSV files
        example: ['simulation/RESNET50_0.2_gradient/model_RESNET50_cifar10_0...a.csv']
    original_XAI: the original XAI file
        example: 'simulation/model_RESNET50_cifar10_XAI.csv'
    prune_XAI: list of all the pruned XAI files
        example: ['simulation/RESNET50_0.2_gradient/model_RESNET50_cifar10_0..._XAI.csv']
    """
    simulations = glob.glob('simulation/*')
    simulations = [i for i in glob.glob('simulation/*') if os.path.isdir(i)]
    
    original_weights = glob.glob("simulation/*.pth")[0]
    original_CSV = glob.glob("simulation/*a.csv")[0]
    try:
        original_XAI = glob.glob("simulation/*_XAI.csv")[0]
    except:
        original_XAI = None

    prune_weights =  glob.glob("simulation/*/*.pth")
    prune_CSV = [i for i in glob.glob("simulation/*/*a.csv")]
    prune_XAI = [i for i in glob.glob("simulation/*/*_XAI.csv")]
    
    
    return simulations, original_CSV, original_weights, prune_weights, prune_CSV, original_XAI, prune_XAI

def getSimulationData(simulations):
    """
    converst the simulations to a dictionary of dataframes
    simulations: list of all the simulations
        example: ['simulation/RESNET50_0.2_gradient', 'simulation/RESNET50_0.2_random', 'simulation/RESNET50_0.2_magnitude']
    returns: a dictionary of dataframes
        example: {'simulation/RESNET50_0.2_gradient': [df1, df2, df3], 'simulation/RESNET50_0.2_random': [df1, df2, df3], 'simulation/RESNET50_0.2_magnitude': [df1, df2, df3]}
    """
    simulation_data = {}
    for simulation in simulations:
        simulation_data[simulation] = [pd.read_csv(i) for i in sorted(glob.glob(simulation + '/*.csv'))]
    # add an epoch column
    for key, value in simulation_data.items():
        for i, df in enumerate(value):
            df['epoch'] = i
    return simulation_data

def plot_prune_rates(simulation_data, model_name, original_csv):
    #  three subplots: gradient pruning, random pruning, magnitude pruning
    fig, axs = plt.subplots(3, 1, figsize=(12, 12))

    index = 0
    axs = axs.flatten()
    #  we want to split by pruning type
    for speed in ["gradient", "random", "magnitude"]:
        ax = axs[index]
        #  for each axis we want to plot the original data and the pruned data
        original_data = pd.read_csv(original_csv)
        original_data['epoch'] = original_data.index
        ax.plot(original_data['epoch'], original_data['train_accuracy'], label='Original', color='black')    

        # we also want to plot the loss on the same axis for the original data
        ax2 = ax.twinx()
        # ax label
        ax.set_xlabel('Epoch')
        ax.set_ylabel('Accuracy', color='blue')
        ax2.set_ylabel('Loss', color='red')
        ax2.plot(original_data['epoch'], original_data['train_loss'], label='Original', color='black', linestyle='dashed')

        # vertical line for the last epoch
        end = original_data['epoch'].iloc[-1]
        ax.axvline(x=end, color='black', linestyle='dashed')
        # Done with the original data

        # get the correct model name
        new_model_name = model_name + ' ' + speed
        matches = [ i for i in simulation_data.keys() if all(x in i for x in new_model_name.split())]
        # matches == 'simulation/gradientMagnitude_RESNET50_conservative_0.2', 'simulation/gradientMagnitude_RESNET50_liberal_0.2', 'simulation/gradientMagnitude_RESNET50_normal_0.2']
        for i in matches:
            # load in the data
            loaded_data = pd.concat([i for i in simulation_data[i]])
            loaded_data = loaded_data.reset_index(drop=True)
            loaded_data['epoch'] = loaded_data.index + end
            ax.plot(loaded_data['epoch'], loaded_data['train_accuracy'], label=i.split("/")[-1])
            ax2.plot(loaded_data['epoch'], loaded_data['train_loss'], label=i.split("/")[-1], linestyle='dashed')
        
        # vertical lines for every 20 epochs
        verticalLines = [i for i in range(end+20, len(loaded_data)+end, 20)]
        for line in verticalLines:
            ax.axvline(x=line, color='black', linestyle='dashed')
        
        # legends outside the plot
        ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        ax2.legend(bbox_to_anchor=(1.05, 0), loc='lower left')
        index += 1
        ax.set_title(new_model_name)
    plt.tight_layout()
    os.makedirs('./plots/prune_rates', exist_ok=True)
    plt.savefig(f"./plots/prune_rates/{model_name}.png")
    plt.close()
   
def analyzeXAI(prune_weights, plotimages, data, original_weights):
    """
    prune_weights: list of all the pruned weights
        example: ['simulation/random_RESNET50_liberal_0.9/model_RESNET50_cifar10_0.19849253305842324_model.pth' ...]
    plotimages: the number of images to plot
    data: dataloader for the dataset
    original_weights: the original weights
        example: 'simulation/model_RESNET50_cifar10.pth'
    """
    def XAI_study(weightFile, data,model=modelBuilder.modelBuilder("RESNET50", "cifar10", False), algorithm=captum.IntegratedGradients):
        """
        weightFile: the weight file
        data: dataloader for the dataset
        model: the model
        algorithm: the algorithm to use for the XAI
        """

        def attribute_image_features(model, algorithm, input, **kwargs):
            model.zero_grad()
            tensor_attributions = algorithm.attribute(input, **kwargs)
            return tensor_attributions

        model.load_state_dict(torch.load(weightFile), strict=False)
        model.eval()

        integrated_gradients = algorithm(model)

        results = pd.DataFrame(columns=["image", "label", "prediction", "confidence", "attribution", "delta", "shape"])
        # for every image in the dataset get the attribution using Integrated Gradients
        for batch in tqdm.tqdm(data):
            images, labels = batch
            for image, label in zip(images, labels):
                image = image.unsqueeze(0)
                image.requires_grad = True
                # get the prediction
                prediction = model(image)
                # get the label
                pred_label_idx = prediction.argmax().item()
                
                # Use a black image as the baseline for occlusion
                baseline = torch.zeros_like(image)

                attr_ig = attribute_image_features(model, integrated_gradients, image, baselines=baseline, target=pred_label_idx, return_convergence_delta=True)
                attr_ig = np.transpose(attr_ig[0].squeeze().cpu().detach().numpy(), (1, 2, 0))
                delta = abs(attr_ig[1])
                confidence = prediction.max().item()
            
                results = pd.concat([results, pd.DataFrame({"image": [image.detach().numpy()], "label": [label], "prediction": [pred_label_idx], "confidence": [confidence], "attribution": [attr_ig], "delta": [delta]})])

        # return the results as a dataframe Index(['image', 'label', 'prediction', 'confidence', 'attribution', 'delta', 'shape'],
        return results

        
    def plot_xai_results(results, filename, num=10):
        for row in range(len(results)):
            if row > num:
                break
            imageData = results.iloc[row]
            image, label, prediction, confidence, attribution, delta, shape = imageData
            image = image[0].transpose(1, 2, 0)

            fig, axs = plt.subplots(1, 3, figsize=(15, 5))
            axs = axs.flatten()
            axs[0].imshow(image)
            axs[0].set_title(f"Label: {label}")
            # convert the attribution to grayscale
            attribution = np.sum(attribution, axis=2)
            axs[1].imshow(attribution, cmap='gray')
            axs[1].set_title(f"Prediction: {prediction} Confidence: {confidence}")
            axs[2].imshow(attribution, cmap='gray')
            axs[2].imshow(image, alpha=0.5)
            axs[2].set_title(f"Occulusion")
            plt.tight_layout()
            os.makedirs(f'./plots/visualization/{filename}', exist_ok=True)
            plt.savefig(f"./plots/visualization/{filename}/{row}.png")
            plt.close()

    def compareResults(originalResults, pruneResults):
        """
        compares the original results to the pruned results and returns the difference
        originalResults: the original results
        pruneResults: the pruned results
        """

        original_features = originalResults['attribution'].values
        prune_features = pruneResults['attribution'].values
        original_features = np.array([i for i in original_features])
        prune_features = np.array([i for i in prune_features])

        for i, (originalFeature, pruneFeature) in enumerate(zip(original_features, prune_features)):
            # calculate the cosine similarity
            cosine_similarity = np.dot(originalFeature.flatten(), pruneFeature.flatten()) / (np.linalg.norm(originalFeature.flatten()) * np.linalg.norm(pruneFeature.flatten()))
            # calculate the euclidean distance
            euclidean_distance = np.linalg.norm(originalFeature.flatten() - pruneFeature.flatten())
            # calculate the mean absolute error
            mean_absolute_error = np.mean(np.abs(originalFeature.flatten() - pruneFeature.flatten()))
            # calculate the mean squared error
            mean_squared_error = np.mean((originalFeature.flatten() - pruneFeature.flatten())**2)
            # calculate the mean absolute percentage error
            # calculate the peak signal to noise ratio
            peak_signal_to_noise_ratio = 20 * np.log10(255 / np.sqrt(mean_squared_error))

            # save the results
            pruneResults.loc[i, 'cosine_similarity'] = cosine_similarity
            pruneResults.loc[i, 'euclidean_distance'] = euclidean_distance
            pruneResults.loc[i, 'mean_absolute_error'] = mean_absolute_error
            pruneResults.loc[i, 'mean_squared_error'] = mean_squared_error
            pruneResults.loc[i, 'peak_signal_to_noise_ratio'] = peak_signal_to_noise_ratio

        return pruneResults
    
    originalResults = XAI_study(weightFile=original_weights, data=data)
    filename = original_weights[original_weights.find("/")+1:-4]
    originalResults.to_csv(f"{original_weights[:-4]}_XAI.csv")
    if plotimages>0:
        plot_xai_results(originalResults, filename, plotimages)

    for prune_weight in tqdm.tqdm(prune_weights):
        # Get the filename
        filename = prune_weight[prune_weight.find("/")+1:-4]
        # get the XAI results for the data
        pruneResults = XAI_study(weightFile=prune_weight, data=data)

        if plotimages>0:
            plot_xai_results(pruneResults, filename, plotimages)

        pruneResults = compareResults(originalResults, pruneResults)
        # save the results to a csv file where the filename is the name of the prune_weight
        print(f"saving {prune_weight[:-4]}_XAI.csv")
        pruneResults.to_csv(f"{prune_weight[:-4]}_XAI.csv")

def plot_similarity_rates(prune_XAI, model_name, plotimages=10, error_type="peak_signal_to_noise_ratio"):
    """
    prune_XAI: list of all the pruned XAI files
        example: ['simulation/RESNET50_0.2_gradient/model_RESNET50_cifar10_0..._XAI.csv']
    model_name: the name of the model
    """
    fig, axs = plt.subplots(3, 1, figsize=(15, 15))
    index = 0
    axs = axs.flatten()
    for speed in ["gradientMagnitude", "random", "magnitude"]:
        ax = axs[index]
        # get the correct model name
        new_model_name = model_name + ' ' + speed
        names = np.unique([i.split("/")[1] for i in prune_XAI])
        folderpath = [i for i in names if all(x in i for x in new_model_name.split())]
        # ['gradientMagnitude_RESNET50_conservative_0.2', 'gradientMagnitude_RESNET50_liberal_0.2', 'gradientMagnitude_RESNET50_normal_0.2']
        for study in folderpath:
            experiement = sorted(glob.glob(f"simulation/{study}/*_XAI.csv"))
            data = []
            for file in experiement:
                data.append(pd.read_csv(file)[error_type].median())
            ax.plot(data, label=study.split("_")[-2])
            print(study)
            
        ax.set_xlabel('Epoch')
        ax.set_ylabel('Accuracy', color='blue')
        ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        index += 1
        ax.set_title(new_model_name + " " + error_type)
    plt.tight_layout()
    

    os.makedirs(f'./plots/similarity_rates/{error_type}', exist_ok=True)
    plt.savefig(f"./plots/similarity_rates/{error_type}/{model_name}.png")
    plt.close()

def main():
    if torch.cuda.is_available():
        print("CUDA is available")
        os.makedirs('./plots', exist_ok=True)
    else:
        print("CUDA is not available")
        quit()

    # get the files
    simulations, original_CSV, original_weights, prune_weights, prune_CSV, original_XAI, prune_XAI = getFiles()
    # convert the simulations to a dictionary of dataframes
    simulation_data = getSimulationData(simulations)
    
    # This is where we save all data   
    os.makedirs('./plots', exist_ok=True)
    # For every percentage of pruning rate we want to plot the prune rates
    for percentage in ["0.2", "0.4", "0.6", "0.8", "0.9"]:
        plot_prune_rates(simulation_data, f"RESNET50 {percentage}", original_CSV)
    

    # This is our golden standard for the XAI
    _, data, _ = dataset.datasetBuilder("cifar10", 10, 1, 0.5, 0.001, 0.499, 6991)
    data = torch.utils.data.DataLoader(data.dataset, batch_size=1, shuffle=False)

    plotimages = 10
    analyzeXAI(prune_weights, plotimages, data, original_weights)


    simulations, original_CSV, original_weights, prune_weights, prune_CSV, original_XAI, prune_XAI = getFiles()
    # For every percentage of pruning rate we want to plot the prune rates
    for error_type in ["peak_signal_to_noise_ratio", "cosine_similarity", "euclidean_distance", "mean_absolute_error", "mean_squared_error"]:
        for percentage in ["0.2", "0.4", "0.6", "0.8", "0.9"]:
            plot_similarity_rates(prune_XAI, f"RESNET50 {percentage}", plotimages=10, error_type=error_type)
        

if __name__ == "__main__":
    main()