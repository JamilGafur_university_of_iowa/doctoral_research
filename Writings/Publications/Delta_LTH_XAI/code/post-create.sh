#! /usr/bin/env bash

# This script is executed inside post-create VM hook.
conda install anaconda -y
pip install -r requirements.txt
conda update -n base -c defaults conda -y
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia -y
conda install captum -c pytorch -y