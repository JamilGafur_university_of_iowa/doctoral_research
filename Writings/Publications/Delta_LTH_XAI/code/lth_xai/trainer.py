import torch
import tqdm

def process_data(model, data_loader, optimizer, criterion, device, mode):
    """
    Process the data using the provided model, optimizer, and criterion.

    Parameters:
        model (torch.nn.Module): The model to process the data with.
        data_loader (torch.utils.data.DataLoader): Dataloader for the dataset.
        optimizer (torch.optim.Optimizer): Optimizer for updating model parameters.
        criterion (torch.nn.Module): Loss function.
        device (torch.device): Device to run the model on.
        mode (str): 'train' or 'eval' mode for the model.

    Returns:
        epoch_loss (float): Total loss for the epoch.
        correct_predictions (int): Number of correctly predicted samples.
        total_samples (int): Total number of samples processed.
    """
    model.train() if mode == 'train' else model.eval()  # Set model mode
    epoch_loss = 0
    correct_predictions = 0
    total_samples = 0

    with torch.set_grad_enabled(mode == 'train'):  # Enable gradients only during training
        for data, target in tqdm.tqdm(data_loader, desc=mode, leave=False):
            data, target = data.to(device), target.to(device)
            if optimizer:
                optimizer.zero_grad()
            output = model(data)
            loss = criterion(output, target)
            if optimizer:
                loss.backward()
                optimizer.step()

            epoch_loss += loss.item()

            _, predicted = output.max(1)
            total_samples += target.size(0)
            correct_predictions += predicted.eq(target).sum().item()

    return epoch_loss, correct_predictions, total_samples


def train(model, train_loader, val_loader, test_loader, epochs, optimizer, criterion, device=torch.device("cpu")):
    """
    Train the given model using the provided data loaders and return the trained model and evaluation metrics.

    Parameters:
        model (torch.nn.Module): The model to be trained.
        train_loader (torch.utils.data.DataLoader): Dataloader for the training set.
        val_loader (torch.utils.data.DataLoader): Dataloader for the validation set.
        test_loader (torch.utils.data.DataLoader): Dataloader for the test set.
        epochs (int): Number of epochs to train the model.
        optimizer (torch.optim.Optimizer): Optimizer for updating model parameters.
        criterion (torch.nn.Module): Loss function.
        device (torch.device): Device to run the model on.

    Returns:
        model (torch.nn.Module): Trained model.
        train_accuracy (list): List of training accuracies for each epoch.
        test_accuracy (list): List of test accuracies for each epoch.
        val_accuracy (list): List of validation accuracies for each epoch.
        train_loss (list): List of training losses for each epoch.
        test_loss (float): Average test loss.
        val_loss (float): Average validation loss.
    """
    model.to(device)

    train_accuracy, test_accuracy, val_accuracy, train_loss, test_loss, val_loss = [], [], [], [], [], []


    for epoch in tqdm.tqdm(range(epochs), desc="Training"):
        # Training Phase
        train_loss_epoch, correct_train, total_train = process_data(model, train_loader, optimizer, criterion, device, 'train')
        train_acc = correct_train / total_train
        train_accuracy.append(train_acc)
        train_loss.append(train_loss_epoch / len(train_loader))

        # Validation Phase
        val_loss_epoch, correct_val, total_val = process_data(model, val_loader, None, criterion, device, 'eval')
        val_acc = correct_val / total_val
        val_accuracy.append(val_acc)
        val_loss.append(val_loss_epoch / len(val_loader))

        # Testing Phase
        test_loss_epoch, correct_test, total_test = process_data(model, test_loader, None, criterion, device, 'eval')
        test_acc = correct_test / total_test
        test_accuracy.append(test_acc)
        test_loss.append(test_loss_epoch / len(test_loader))


    return model, train_accuracy, test_accuracy, val_accuracy, train_loss, test_loss, val_loss

