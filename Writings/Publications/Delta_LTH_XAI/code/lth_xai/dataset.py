import torch
import torchvision.transforms as transforms
import random
from torch.utils.data import DataLoader, ConcatDataset
from torchvision.datasets import ImageFolder
from torch.utils.data import Subset
import torchvision.datasets as datasets
import os
import glob
import numpy as np

def datasetBuilder(datasetName: str,
                   batchSize: int, 
                   numWorkers: int, 
                   trainSize: float, 
                   valSize: float, 
                   testSize: float, 
                   seed: int
                   ):
    """
    Takes in a string and returns a dataset split into train, val, test
    INPUT:
        :param datasetName: name of dataset to be built
        :param batchSize: batch size for dataloader
        :param numWorkers: number of workers for dataloader
        :param trainSize: size of training set
        :param valSize: size of validation set
        :param testSize: size of test set
        :param seed: seed for random number generator
    OUTPUT:
        :return train_loader: dataloader for training set
        :return val_loader: dataloader for validation set
        :return test_loader: dataloader for test set
    """
    try: 
        if datasetName == "cifar10":
            dataset = datasets.CIFAR10('./', train=True, download=True, transform=transforms.ToTensor())
        elif datasetName == "cifar100":
            dataset = datasets.CIFAR100('./', train=True, download=True, transform=transforms.ToTensor())
        elif datasetName == "mnist":
            dataset = datasets.MNIST('./', train=True, download=True, transform=transforms.ToTensor())
        elif datasetName == "tinyimagenet":
            # Define transforms
            train_transform = transforms.Compose([
                transforms.RandomResizedCrop(64),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
            ])
            val_transform = transforms.Compose([
                transforms.Resize(64),
                transforms.CenterCrop(64),
                transforms.ToTensor(),
            ])
            # Define paths
            train_path = './tinyimagenet/train'
            val_path = './tinyimagenet/val'
            # Create datasets
            train_dataset = ImageFolder(train_path, transform=train_transform)
            val_dataset = ImageFolder(val_path, transform=val_transform)
            # Merge train and val datasets
            dataset = ConcatDataset([train_dataset, val_dataset])
        else:
            raise ValueError(f"Dataset: {datasetName} not found.\n\t Please choose from: cifar10, cifar100, mnist, tinyimagenet")
    except Exception as e:
        print(f"Error in dataset: {e}")
        quit()
    
    # split dataset into train, val, test
    train_dataset, val_dataset, test_dataset = _datasetSplitter(dataset, trainSize, valSize, testSize, seed)

    # create dataloaders
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batchSize, num_workers=numWorkers, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batchSize, num_workers=numWorkers, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batchSize, num_workers=numWorkers, shuffle=True)

    return train_loader, val_loader, test_loader

    
def _datasetSplitter(dataset: torch.utils.data.Dataset, 
                    train_size: float, 
                    val_size: float, 
                    test_size: float, 
                    seed: int
                    ):
    """
    Takes in a dataset and returns a dataset split into train, val, test
    INPUT:
        :param dataset: dataset to be split
        :param train_size: size of training set
        :param val_size: size of validation set
        :param test_size: size of test set
        :param seed: seed for random number generator
    OUTPUT:
        :return train_dataset: dataset for training set
        :return val_dataset: dataset for validation set
        :return test_dataset: dataset for test set
    """
    try:
        # set seed for reproducibility
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        
        # split dataset into train, val, test
        train_size = int(train_size * len(dataset))
        val_size = int(val_size * len(dataset))
        test_size = int(test_size * len(dataset))
        train_dataset, val_dataset, test_dataset = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
        
        return train_dataset, val_dataset, test_dataset
    
    except Exception as e:
        print(f"Error in datasetSplitter: {e}")
        quit()
   
