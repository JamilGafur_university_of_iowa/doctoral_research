import torch
import torch.nn as nn
import torch.nn.utils.prune as prune
import copy
from captum.attr import *
import random
import tqdm 
def simpleMNISTConvNet():
    return nn.Sequential(
        nn.Conv2d(1, 32, kernel_size=3, stride=1, padding=1),
        nn.ReLU(),
        nn.MaxPool2d(kernel_size=2),
        nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(),
        nn.MaxPool2d(kernel_size=2),
        nn.Flatten(),
        nn.Linear(7 * 7 * 64, 128),
        nn.ReLU(),
        nn.Linear(128, 10),
    )

def _unstructured_prune(weights, pruning_rate):
    """
    return a mask with pruning_rate% of the weights set to 0
    ex: if pruning_rate = .1, then 10% of the lowest abs weights will be set to 0

    Parameters:
        weights (list of torch.Tensor): List containing the weights
        of linear and convolutional modules.
        pruning_rate (float): Pruning rate.

    Returns:
        mask (list of torch.Tensor): List containing the masks
        of linear and convolutional modules.
    """
    flatten_weights = torch.cat([w.flatten() for w in weights])
    abs_weights = torch.abs(flatten_weights)
    k = int(pruning_rate * len(abs_weights))
    threshold = torch.kthvalue(abs_weights, k).values
    mask = [torch.where(torch.abs(w) < threshold, torch.zeros_like(w), torch.ones_like(w)) for w in weights]

    # assert that the sum of the mask is equal to the number of weights that should be pruned
    # assert torch.isclose(torch.tensor(pruning_rate), 1-torch.sum(torch.cat([w.flatten() for w in mask]))/len(flatten_weights)), f"{torch.sum(torch.cat([w.flatten() for w in mask]))/len(flatten_weights)}% of the mask are non-zero"
    # print(f"\t\t\t{torch.sum(torch.cat([w.flatten() for w in mask]))/len(flatten_weights)}% of the mask are non-zero")
    return mask

def _random_prune(weights, pruning_rate):
    """
    Applies random pruning to a list of weights
    according to the specified pruning rate.

    Parameters:
        weights (list of torch.Tensor): List containing the weights
        of linear and convolutional modules.
        pruning_rate (float): Pruning rate.

    Returns:
        mask (list of torch.Tensor): List containing the masks
        of linear and convolutional modules.
    """
    flatten_weights = torch.cat([w.flatten() for w in weights])
    num_weights = len(flatten_weights)

    # Generate a torch of ones in the same shape as the flatten_weights
    mask = torch.ones_like(flatten_weights)
    # generate num_weights random indices between 0 and num_weights
    # and set the values at those indices to 0
    mask[random.sample(range(num_weights), int(num_weights * pruning_rate))] = 0

    # reshape the mask to the shape of the flatten_weights
    mask = [ m.reshape(w.shape) for m, w in zip(mask.split([w.numel() for w in weights]), weights)]


    return mask

def _get_recursive_weights(model):
    """
    Recursively traverses a PyTorch model and retrieves weights
    from linear or convolutional modules.

    Parameters:
        model (nn.Module): PyTorch model to be traversed.

    Returns:
        weights (list of torch.Tensor): List containing the weights
        of linear and convolutional modules.
    """
    weights = []

    def is_linear_or_conv(module):
        return isinstance(module, (nn.Linear, nn.Conv2d)) and hasattr(module, "weight")

    def traverse(module):
        for child_module in module.children():
            if is_linear_or_conv(child_module):
                weights.append(child_module.weight)
            traverse(child_module)

    traverse(model)
    return weights

def _get_recursive_gradients(model: nn.Module,
                            dataloader: torch.utils.data.DataLoader):
    """
    Recursively traverses a PyTorch model and retrieves gradients
    from linear or convolutional modules.

    Parameters:
        model (nn.Module): PyTorch model to be traversed.
        dataloader (torch.utils.data.DataLoader): Dataloader to be used for
        computing gradients.

    Returns:
        gradients (list of torch.Tensor): List containing the gradients
        of linear and convolutional modules.
    """
    # create the gradients
    model = model.to('cpu')
    model.zero_grad()
    loss = nn.CrossEntropyLoss()
    for batch in dataloader:
        x, y = batch
        x = x.to('cpu')
        y = y.to('cpu')
        output = model(x)
        loss(output, y).backward()
        break

    gradients = []

    def is_linear_or_conv(module):
        return isinstance(module, (nn.Linear, nn.Conv2d)) and hasattr(module, "weight")

    def traverse(module):
        for child_module in module.children():
            if is_linear_or_conv(child_module):
                gradients.append(child_module.weight.grad)
            traverse(child_module)

    traverse(model)
    return gradients

def _remove_pruning(module):
    """
    Removes pruning reparameterization from a PyTorch module.

    Parameters:
        module (nn.Module): PyTorch module.
    """
    for child_name, child_module in module.named_children():
        if isinstance(child_module, (nn.Linear, nn.Conv2d)):
            prune.remove(child_module, 'weight')

def _custom_from_mask(module, mask):
    """
    Applies a custom mask to a PyTorch module using the
    torch.nn.utils.prune.custom_from_mask function.

    Parameters:
        module (nn.Module): PyTorch module.
        mask (list of torch.Tensor): List containing the masks
        of linear and convolutional modules.
    """
    def is_linear_or_conv(child_module):
        return isinstance(child_module, (nn.Linear, nn.Conv2d)) and hasattr(child_module, "weight")

    def apply_mask(child_module, layer_mask):
        for name, param in list(child_module.named_parameters()):
            if name == "weight":
                prune.custom_from_mask(child_module, name=name, mask=layer_mask)

    def traverse(child_module):
        params_to_modify = []

        for child_name, grandchild_module in child_module.named_children():
            params_to_modify.extend(traverse(grandchild_module))

        if is_linear_or_conv(child_module):
            layer_mask = mask.pop(0)
            params_to_modify.append((child_module, layer_mask))

        return params_to_modify

    params_to_modify = traverse(module)

    for child_module, layer_mask in params_to_modify:
        apply_mask(child_module, layer_mask)

    _remove_pruning(module)  # Remove pruning reparameterization

def modelApplyMask(model, pruning_rate, pruning_type, device, training_data):
    """
    Applies a mask to the weights of a PyTorch model
    based on the specified pruning rate and type.

    Parameters:
        model (nn.Module): PyTorch model to be pruned.
        pruning_rate (float): Pruning rate.
        pruning_type (str): Pruning type. Must be one of "unstructured" or "random".
        device (torch.device): Device to which the model is transferred.

    Returns:
        original_model (nn.Module): Original PyTorch model.
        pruned_model (nn.Module): Pruned PyTorch model.
        is_pruned (bool): True if pruning was successful, False otherwise.
    """
    model = copy.deepcopy(model)
    model.to(device)

    mask = None

    weights = _get_recursive_weights(model)
    gradients = _get_recursive_gradients(model, training_data)

    if pruning_type == "magnitude":
        mask = _unstructured_prune(weights, pruning_rate)
    elif pruning_type == "random":
        mask = _random_prune(weights, pruning_rate)
    elif pruning_type == "gradientMagnitude":    
        mask = _unstructured_prune(gradients, pruning_rate)
    else:
        raise ValueError(f"Pruning type {pruning_type} is not supported.")

    
    if mask is not None:
        _custom_from_mask(model, mask)

    prunedWeights = _get_recursive_weights(model)
    # if it is not zero it is one
    prunedWeights = [torch.where(w != 0, torch.ones_like(w), torch.zeros_like(w)) for w in prunedWeights]
    prunedWeights = torch.cat([w.flatten() for w in prunedWeights])
    
    # assert torch.isclose(torch.tensor(pruning_rate), 1-torch.sum(prunedWeights)/len(prunedWeights)), f"{torch.sum(prunedWeights)/len(prunedWeights)}% of the models weights after pruning are non-zero"
    return model
