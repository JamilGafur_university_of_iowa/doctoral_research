import torch.nn as nn
import torchvision.models as models
import torch

def modelBuilder(modelName: str, datasetName: str, Weights: bool = True):
    """
    Loads a pre-trained model with adaptations for different datasets.

    Args:
        model_name (str): Name of the model architecture (e.g., "RESNET50", "VGG19", "GOOGLENET").
        dataset_name (str): Name of the dataset (e.g., "mnist", "cifar10", "cifar100", "imagenet").
        use_weights (bool): Whether to use pre-trained weights or not (default: True).

    Returns:
        torch.nn.Module: A PyTorch model object with appropriate adaptations for the chosen dataset.
    """
    dataset_adaptations = {
        "RESNET50": {
            "cifar10": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(2048, 10)
            },
            "cifar100": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(2048, 100)
            },
            "tinyimagenet": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(2048, 200)
            }
        },
        "GOOGLENET": {
            "cifar10": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(1024, 10)
            },
            "cifar100": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(1024, 100)
            },
            "tinyimagenet": {
                "conv1": nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False),
                "fc": nn.Linear(1024, 200)
            }
        },
        "VGG19": {
            "cifar10": {
                "conv1": nn.Conv2d(3, 64, kernel_size=3, padding=1),
                "fc": nn.Linear(4096, 10)
            },
            "cifar100": {
                "conv1": nn.Conv2d(3, 64, kernel_size=3, padding=1),
                "fc": nn.Linear(4096, 100)
            },
            "tinyimagenet": {
                "conv1": nn.Conv2d(3, 64, kernel_size=3, padding=1),
                "fc": nn.Linear(4096, 200)
            }
        }
    }

    if datasetName not in dataset_adaptations[modelName]:
        raise ValueError(f"Unrecognized dataset name. Available options for {modelName}: {', '.join(dataset_adaptations[modelName].keys())}")

    if modelName == "RESNET50":
        model = models.resnet50(pretrained=Weights)
        model.conv1 = dataset_adaptations[modelName][datasetName]["conv1"]
        model.fc = dataset_adaptations[modelName][datasetName]["fc"]
    elif modelName == "VGG19":
        model = models.vgg19(pretrained=Weights)
        model.features[0] = dataset_adaptations[modelName][datasetName]["conv1"]
        model.classifier[6] = dataset_adaptations[modelName][datasetName]["fc"]
    elif modelName == "GOOGLENET":
        model = models.googlenet(pretrained=Weights)
        model.conv1 = dataset_adaptations[modelName][datasetName]["conv1"]
        model.fc = dataset_adaptations[modelName][datasetName]["fc"]
    else:
        raise ValueError(f"Unrecognized model name. Available options: RESNET50, VGG19, GOOGLENET")

    # Check if the model works with the given input shape
    if datasetName == "cifar10":
        input_shape = (32, 3, 32, 32)
    elif datasetName == "cifar100":
        input_shape = (32, 3, 32, 32)
    elif datasetName == "tinyimagenet":
        input_shape = (32, 3, 64, 64)
    else:
        raise ValueError(f"Unrecognized dataset name. Available options: cifar10, cifar100, tinyimagenet")

    try:
        model(torch.randn(input_shape))
    except Exception as e:
        print(f"Error: {e}")

    return model
