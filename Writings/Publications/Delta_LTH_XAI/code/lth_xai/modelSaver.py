
import torch
import os
import pandas as pd
from typing import Union, List


def save_model(model: torch.nn.Module, save_dir: str = "./", prefix: str = "original",
               **kwargs: Union[float, int, List[float], List[int]]) -> None:
    """
    Save the PyTorch model and associated training results.

    Parameters:
        model (torch.nn.Module): The PyTorch model to be saved.
        save_dir (str): The directory to save the model and results.
        prefix (str): Prefix for the saved files.
        **kwargs: Additional information to be saved.

    Returns:
        None
    """
    os.makedirs(save_dir, exist_ok=True)
    
    model_path = os.path.join(save_dir, f"{prefix}_model.pth")
    torch.save(model.state_dict(), model_path)

    if kwargs:
        results_data = kwargs
        
        data_path = os.path.join(save_dir, f"{prefix}_data.csv")
        df = pd.DataFrame(results_data)
        print(df)
        df.to_csv(data_path, index=False)
        print(f"Saved {prefix.capitalize()} model and training results to {data_path}.")
    else:
        print(f"Saved {prefix.capitalize()} model.")


def prune_and_save_model(model: torch.nn.Module, tr, va, te, pruning_type: str, pruning_rate: float,
                         save_dir: str = "./", prefix: str = "", epochs: int = 2) -> None:
    """
    Prune the model, fine-tune, and save the results.

    Parameters:
        model (torch.nn.Module): The PyTorch model to be pruned.
        tr, va, te: Training, validation, and test datasets.
        pruning_type (str): Type of pruning.
        pruning_rate (float): Rate of pruning.
        save_dir (str): The directory to save the pruned model and results.
        prefix (str): Prefix for the saved files.
        epochs (int): Number of fine-tuning epochs.

    Returns:
        None
    """
    
    pruned_model_path = os.path.join(save_dir, pruning_type, f"pruned_{pruning_rate}_finetune{epochs}_model.pth")
    if os.path.exists(pruned_model_path):
        print(f"Pruned model with type {pruning_type} and {pruning_rate:.4f} sparsity already exists. Skipping.")
        return

    print(f"Pruning with type {pruning_type} and {pruning_rate:.4f} sparsity.")

    pruned_model = modelApplyMask(model, pruning_rate, pruning_type, torch.device("cuda"), tr)

    pruned_model, train_accuracy, test_accuracy, val_accuracy, train_loss, test_loss, val_loss = train(
        pruned_model, tr, va, te, epochs, device=torch.device("cuda"))

    pruning_dir = os.path.join(save_dir, pruning_type)
    os.makedirs(pruning_dir, exist_ok=True)

    save_model(pruned_model, pruning_dir, prefix=f"pruned_{pruning_rate}_finetune{epochs}",
               train_accuracy=train_accuracy, test_accuracy=test_accuracy,
               val_accuracy=val_accuracy, train_loss=train_loss, val_loss=val_loss, test_loss=test_loss)

    print(f"Pruned with type {pruning_type} and {pruning_rate:.4f} sparsity with accuracy: {test_accuracy[-1]}%, "
          f"validation accuracy: {val_accuracy[-1]}%")
