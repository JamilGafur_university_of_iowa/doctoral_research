#! /usr/bin/env bash

# This script is executed inside post-create VM hook.
pip install -r requirements.txt
conda update -n base -c defaults conda -y
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia -y
git clone git@github.com:EpiGenomicsCode/Adversarial_Observation.git
cd Adversarial_Observation/
python setup.py install
conda install -c conda-forge umap-learn
