import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import Swarm_Observer as so
import Adversarial_Observation as AO
import torchvision
import numpy as np


# setup==============
def train(model, train_loader, optimizer, epoch, loss_func=nn.CrossEntropyLoss(), device=torch.device("cuda")):
    model.train()
    # move model to device
    model.to(device)
    
    for batch_idx, (data, target) in enumerate(train_loader):
        # Forward pass
        data, target = data.to(device), target.to(device)
        output = model(data)
        loss = loss_func(output, target)

        # Backward pass
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Print progress
        if batch_idx % 100 == 0:
            print(f"Train Epoch: {epoch} [{batch_idx * len(data)}/{len(train_loader.dataset)} "
                  f"({100. * batch_idx / len(train_loader):.0f}%)]\tLoss: {loss.item():.6f}")

def test(model, test_loader, device=torch.device("cuda")):
    model.eval()
    test_loss = 0
    correct = 0
    # move model to device
    model.to(device)

    # Don't calculate gradients
    with torch.no_grad():
        for data, target in test_loader:
            # Move data to device
            data, target = data.to(device), target.to(device)
            # Forward pass
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction="sum").item()

            # Get the index of the max log-probability
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    # Print results
    test_loss /= len(test_loader.dataset)
    print(f"\nTest set: Average loss: {test_loss:.4f}, Accuracy: {correct}/{len(test_loader.dataset)} "
          f"({100. * correct / len(test_loader.dataset):.0f}%)\n")

    accuracy = correct / len(test_loader.dataset)
    return accuracy

def get_CIFAR10_DataLoaders():
    # Load MNIST dataset with proper transformations
    train_loader = datasets.CIFAR10("./data", train=True, download=True, transform=transforms.ToTensor())

    test_loader = datasets.CIFAR10("./data", train=False, download=True, transform=transforms.ToTensor())

    train_loader = torch.utils.data.DataLoader(train_loader, batch_size=1024, shuffle=True)

    test_loader = torch.utils.data.DataLoader(test_loader, batch_size=1024, shuffle=True)

    return train_loader, test_loader

def get_Resnet_Model():
    """
    Returns the pretrained resnet model
    """
    return torchvision.models.resnet152(pretrained=True)

# experiments==============
def experiment1(model, test_loader, cutoff=0.9):
    """
    The goal of this experiment is to:
        1. generate the heatmap of the first 20 images in the test set
        2. generate Adversarial images for the first 20 images in the test set
        3. generate the heatmap of the adversarial images
    """
    import seaborn as sns
    import matplotlib.pyplot as plt
    import numpy as np
    import os

    sns.set_style("darkgrid")
    sns.set_context("paper")
    sns.set(font_scale=1.5)
    sns.set_style("ticks")

    def clean_memory():
        if os.path.exists("./experiment_1/"):
            import shutil
            shutil.rmtree("./experiment_1/")
        os.makedirs("./experiment_1/", exist_ok=True)

        # clean memory
        import gc
        gc.collect()
        torch.cuda.empty_cache()
        
    def get_n_inputs(n, test_loader):
        """
        Returns the first n inputs from the test loader
        """
        inputs = []
        labels = []
        # get the first batch, iterate through the batch adding each image to the list
        for batch_idx, (datas, targets) in enumerate(test_loader):
            for data, target in zip(datas, targets):
                inputs.append(data)
                labels.append(target)
                if len(inputs) >= n:
                    return inputs, labels
    
    def generate_adversarial_images(model, images, labels):
        """
        Generates adversarial images for the given images and labels
        AO.Attacks.fgsm_attack(data[0].unsqueeze(0), model, (1,3, 32, 32), 0.1)
        """

        adversarial_images = []
        for image, label in zip(images, labels):
            adver_image = AO.Attacks.fgsm_attack(image.unsqueeze(0), model, (1, 3, 32, 32), 0.1)[0]
            adver_image = torch.tensor(adver_image).to("cpu").to(torch.float32).unsqueeze(0)

            # if it is misclassified add it to the list
            if model.to('cpu')(adver_image).argmax() != label:
                adversarial_images.append(adver_image.squeeze())

        return adversarial_images
    
    def generate_heatmap(model, images, labels, dir, name, cutoff=0.9):
        os.makedirs(f"./experiment_1/{dir}", exist_ok=True)
        os.makedirs(f"./experiment_1/{dir}/plots", exist_ok=True)
        os.makedirs(f"./experiment_1/{dir}/data", exist_ok=True)
        
        # generate heatmap for each image and save the image and heatmap
        for i, (image, label) in enumerate(zip(images, labels)):
            # get the heatmap
            Saliency = AO.Attacks.saliency_map(image, model, label)
            Saliency = Saliency.cpu().detach().numpy()
            Saliency = np.squeeze(Saliency)
            
            # save the original image and the heatmap as npy files
            np.save(f"./experiment_1/{dir}/data/heatmap_label_{label}_index_{i}_name_{name}.npy", Saliency)
            np.save(f"./experiment_1/{dir}/data/image_label_{label}_index_{i}_name_{name}.npy", image.cpu().detach().numpy())
            
            # plot the rgb image and the heatmap
            fig, ax = plt.subplots(1,2, figsize=(20, 10))
            # convert image to numpy
            image = image.cpu().detach().numpy().squeeze()

            # convert to rgb 
            image = np.moveaxis(image, 0, -1)
            Saliency = np.moveaxis(Saliency, 0, -1)
            # sum up the saliency map across the channels, this will give us a 2D heatmap
            # convert to binary, anything over 90% deviation from the mean is considered important
            Saliency = np.sum(Saliency, axis=-1)
            Saliency = (Saliency > Saliency.mean() + cutoff * Saliency.std()).astype(np.float32)
            Saliency = Saliency.reshape(32,32,1)*image


            # plot the images
            ax[0].imshow(image)
            ax[1].imshow(Saliency*image)

            # hid ticks
            ax[0].set_xticks([])
            ax[0].set_yticks([])
            ax[1].set_xticks([])
            ax[1].set_yticks([])
            # remove the whitespace from the top and bottom
            fig.subplots_adjust(top=1, bottom=0, hspace=0, wspace=0.1)


            # save the figure
            plt.savefig(f"./experiment_1/{dir}/plots/heatmap_label_{label}_index_{i}_name_{name}.png")
            plt.close()

    # get the first 20 images and labels
    images, labels = get_n_inputs(20, test_loader)

    # get the first 20 adversarial images
    adversarial_images = generate_adversarial_images(model, images, labels)

    # make the directories if they exist remove them and remake them
    clean_memory()

    # generate the heatmap for the original images
    generate_heatmap(model, images, labels, "original", "original")

    # generate the heatmap for the adversarial images
    generate_heatmap(model, adversarial_images, labels, "adversarial", "adversarial")

def experiment2(model, train_loader, test_loader, datasize=10):
    
    def getUMAPEmbedding(data, labels):
        import umap
        import matplotlib.pyplot as plt
        import numpy as np
        import pandas as pd
        import seaborn as sns
        import os
        sns.set_style("darkgrid")
        sns.set_context("paper")

        # train a UMAP model on the MNIST dataset with 2 dimensions
        umap_model = umap.UMAP()
        umap_model.fit(data.reshape(len(data), -1), y=labels)
        
        # get the 2D embeddings
        embeddings = umap_model.transform(data.reshape(len(data), -1))
        
        return embeddings, umap_model
    
    def Swarm_analysis(model, starting_positions,epochs=10, cost_out=3):

        def cost_func(model, position):
            device = torch.device("cuda")
            model.to(device)
            position = position.to(device)
            # get the output of the model
            return model(position.unsqueeze(0))[0][cost_out].item()

        # step 2: define swarm
        swarm = so.PSO(torch.tensor(starting_positions), cost_func, model)

        # step 3: run swarm
        swarm.run(epochs)

        # get history
        history = swarm.get_history()

        # print the best position
        print(f"Best Position: {swarm.cos_best_g}")

        return history

    def flatten_Dataloader(loader):
        data = []
        targets = []
        for batch_idx, (d, t) in enumerate(test_loader):
            data.append(d)
            targets.append(t)

        data = torch.cat(data)
        targets = torch.cat(targets)
        return data, targets
    
    def plot_swarm_movement(embeddings, labels, history, umap_model, cost_out=3):
        """
        Plot the embeddings and their respective labels. for each epoch in history plot the swarm as red triangle with dashed black arrows pointing tot he next position
        """
        import matplotlib.pyplot as plt
        import numpy as np
        import seaborn as sns
        import os
        sns.set_style("darkgrid")
        sns.set_context("paper")

        os.makedirs("./experiment_2/plots", exist_ok=True)


        # plot the embeddings
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        # plot the embeddings and their labels 
        
        # for i in range all unique labels plot the embeddings with the label
        for i in np.unique(labels):
            # get the embeddings for the label
            # if i == cost_out:
            label_embeddings = embeddings[labels == i]
            # plot the embeddings
            ax.scatter(label_embeddings[:, 0], label_embeddings[:, 1], label=f"{i}", s=100)
            
        
        # plot the swarm
        import tqdm 
        for i in tqdm.tqdm(range(0, len(history.keys()) - 2)):
            # get the points for the ith epoch
            points = history[f"epoch_{i}"]
            # get the points for the ith epoch
            next_points = history[f"epoch_{i+1}"]
            # embed the points
            points = [umap_model.transform(point.reshape(-1,3*32*32)) for point in points]
            next_points = [umap_model.transform(point.reshape(-1,3*32*32)) for point in next_points]
            # plot the points
            points = np.vstack(points)
            next_points = np.vstack(next_points)
            # if it is the last epoch include the label "swarm"
            if i == len(history.keys()) - 3:
                ax.scatter(points[:, 0], points[:, 1], color="red", s=100, label="Swarm")
            else:
                ax.scatter(points[:, 0], points[:, 1], color="red", s=100)
            # plot the arrows
            for point, next_point in zip(points, next_points):
                ax.arrow(point[0], point[1], next_point[0] - point[0], next_point[1] - point[1], color="black", linestyle="--", width=0.01, head_width=0.5, head_length=0.5)

        # add the legend
        ax.legend()
        # set the title
        ax.set_title("Swarm Movement")
        # save the figure
        plt.savefig("./experiment_2/plots/swarm_movement.png")
        plt.close()
        # save the embeddings label and history
        np.save("./experiment_2/embeddings.npy", embeddings)
        np.save("./experiment_2/labels.npy", labels)
        np.save("./experiment_2/history.npy", history)

    # set the cost out
    cost_out = 3 

    # get the data and targets
    all_data, all_targets = flatten_Dataloader(train_loader)
    embeddings, umap_model = getUMAPEmbedding(all_data, all_targets)

    # shuffle the data and targets that is not classified as a 3 
    data = all_data[all_targets != cost_out]
    targets = all_targets[all_targets != cost_out]
    idx = np.random.permutation(len(data))
    data = data[idx][:datasize]
    targets = targets[idx][:datasize]
    
    history = Swarm_analysis(model, data, epochs=10, cost_out=cost_out)

    plot_swarm_movement(embeddings, all_targets, history, umap_model)

def experiment3(train_loader, test_loader,n_inputs=10):

    def get_n_inputs(n, test_loader):
        """
        Returns the first n inputs from the test loader
        """
        inputs = []
        labels = []
        # get the first batch, iterate through the batch adding each image to the list
        for batch_idx, (datas, targets) in enumerate(test_loader):
            for data, target in zip(datas, targets):
                inputs.append(data)
                labels.append(target)
                if len(inputs) >= n:
                    return inputs, labels
        
    def generate_adversarial_images(model, images, labels):
        """
        Generates adversarial images for the given images and labels
        AO.Attacks.fgsm_attack(data[0].unsqueeze(0), model, (1,3, 32, 32), 0.1)
        """
        import tqdm
        adversarial_images = []
        for image, label in tqdm.tqdm(zip(images, labels), total=len(images)):
            adver_image = AO.Attacks.fgsm_attack(image.unsqueeze(0), model, (1, 3, 32, 32), 0.1)[0]
            adver_image = torch.tensor(adver_image).to("cpu").to(torch.float32).unsqueeze(0)

            # if it is misclassified add it to the list
            if model.to('cpu')(adver_image).argmax() != label:
                adversarial_images.append(adver_image.squeeze())

        return adversarial_images
   
    def calc_misclassification(model, adversarial_images, real_labels):
        """
        Returns the number of misclassified images
        """
        import tqdm
        misclassified = 0
        for image, label in tqdm.tqdm(zip(adversarial_images, real_labels), total=len(adversarial_images)):
            if model.to('cpu')(image.unsqueeze(0)).argmax() != label:
                misclassified += 1
        return misclassified

    model = get_Resnet_Model()    
    images, labels = get_n_inputs(n_inputs, test_loader)
    adversarial_images = generate_adversarial_images(model, images, labels)
    misclassified = calc_misclassification(model, adversarial_images, labels)
    print(f"percentage misclassified: {misclassified/len(adversarial_images)}")

    # split adversarial images into train and test
    test_images_adv = adversarial_images[:int(len(adversarial_images)/2)]
    test_labels_adv = labels[:int(len(adversarial_images)/2)]
    train_images_adv = adversarial_images[int(len(adversarial_images)/2):]
    train_labels_adv = labels[int(len(adversarial_images)/2):]

    # add the misclassified and their labels to the train_loader
    new_data = np.concatenate((train_loader.dataset.data, torch.tensor(np.array(train_images_adv)).permute(0,2,3,1).to(torch.float32)))
    new_targets = np.concatenate((train_loader.dataset.targets, np.array(train_labels_adv)))

    # create the new dataset
    new_dataset = torch.utils.data.TensorDataset(torch.tensor(new_data).permute(0,3,1,2).to(torch.float32), torch.tensor(new_targets))
    # create the new dataloader
    train_loader = torch.utils.data.DataLoader(new_dataset, batch_size=1024, shuffle=True)

    # train the model
    model.fc = nn.Linear(2048, 10)    
    misclassified = [100]
    optimizer = torch.optim.Adam(model.parameters())
    for epoch in range(1, 10):
        train(model, train_loader, optimizer, epoch)
        # Test model
        print(f" Test Accuracy: {test(model, test_loader):.4f}")
        update = calc_misclassification(model, test_images_adv, test_labels_adv)
        print(f"percentage misclassified: {update/len(test_images_adv)}")
        misclassified.append(update/len(test_images_adv))

    # make the experiment_3 directory
    import os
    os.makedirs("./experiment_3/", exist_ok=True)
    # save the misclassified
    np.save("./experiment_3/misclassified.npy", misclassified)

    # plot the misclassified
    import seaborn as sns
    import matplotlib.pyplot as plt
    sns.set_style("darkgrid")
    sns.set_context("paper")
    sns.set(font_scale=1.5)
    sns.set_style("ticks")
    plt.plot(misclassified)
    plt.title("Misclassified Adversarial Images")
    plt.xlabel("Epoch")
    plt.ylabel("Percentage Misclassified")
    plt.tight_layout(pad=0)
    plt.savefig("./experiment_3/misclassified.png")
    plt.close()
    

def introduction_adverarial_Attack():
    import torch
    import torch.nn as nn
    import torch.optim as optim
    import torchvision
    from torchvision import transforms
    from torchvision.models import resnet50
    from torch.utils.data import DataLoader
    from torchvision.datasets import CIFAR10
    import matplotlib.pyplot as plt
    import os



    def fgsm_attack(image, epsilon, data_grad):
        perturbed_image = image + epsilon * data_grad.sign()
        perturbed_image = torch.clamp(perturbed_image, 0, 1)
        return perturbed_image

    def generate_adversarial_example(model, criterion, image, label, epsilon):
        image.requires_grad = True

        output = model(image)
        loss = criterion(output, label)
        loss.backward()

        data_grad = image.grad.data

        perturbed_image = fgsm_attack(image, epsilon, data_grad)

        return perturbed_image

    def test_adversarial_example(model, criterion, test_loader, epsilon, class_labels):
        correct = 0
        total = 0
        model.to('cuda')

        if os.path.exists("./adversarial_examples/"):
            import shutil
            shutil.rmtree("./adversarial_examples/")
        for data in test_loader:
            original_image, labels = data
            original_image, labels = original_image.cuda(), labels.cuda()

            # Get the original prediction
            outputs = model(original_image)
            _, original_prediction = torch.max(outputs.data, 1)
            confidence_original = torch.nn.functional.softmax(outputs, dim=1)[0, original_prediction.item()].item()

            # Generate adversarial example
            perturbed_image = generate_adversarial_example(model, criterion, original_image, labels, epsilon)

            # Classify the adversarial example
            outputs = model(perturbed_image)
            _, perturbed_prediction = torch.max(outputs.data, 1)
            confidence_perturbed = torch.nn.functional.softmax(outputs, dim=1)[0, perturbed_prediction.item()].item()

            total += labels.size(0)
            correct += (perturbed_prediction == labels).sum().item()

            # Plot the images
            fig, axs = plt.subplots(1,3, figsize=(15,5))

            axs[0].imshow(original_image.squeeze().cpu().detach().numpy().transpose(1, 2, 0))
            axs[0].axis('off')

            noise = perturbed_image - original_image
            axs[1].imshow(noise.squeeze().cpu().detach().numpy().transpose(1, 2, 0))
            axs[1].axis('off')

            axs[2].imshow(perturbed_image.squeeze().cpu().detach().numpy().transpose(1, 2, 0))
            axs[2].axis('off')

            # add the title to the whole figure
            # increase the title size make it bold
            fig.suptitle(f"Original Prediction: {class_labels[original_prediction.item()]},\n Misclassification: {class_labels[perturbed_prediction.item()]}", fontsize=30)
            # remove the whitespace from the top and bottom
            fig.subplots_adjust(top=1, bottom=0, hspace=0, wspace=0.1)
            plt.tight_layout()
            # remove directory if it exists
            os.makedirs("./adversarial_examples/", exist_ok=True)
            # if the directory has less than 50 images save the image
            if len(os.listdir("./adversarial_examples/")) < 50:
                plt.savefig(f"./adversarial_examples/original_{class_labels[original_prediction.item()]}_perturbed_{class_labels[perturbed_prediction.item()]}_confidence_original_{confidence_original:.4f}_confidence_perturbed_{confidence_perturbed:.4f}.png")
            else:
                break
            plt.close()
            

        

    # Load the pre-trained ResNet-50 model
    
# Load the pre-trained ResNet-50 model
    model = resnet50(pretrained=True)
    # set the output layer to 10
    model.fc = nn.Linear(2048, 10)
    model.eval()

    # Define the transformation for the input images
    transform = transforms.Compose([
        transforms.ToTensor(),
    ])

    # Load the CIFAR-10 test dataset
    test_dataset = CIFAR10(root='./data', train=False, download=True, transform=transform)
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=True)

    # Define the class labels for CIFAR-10
    class_labels = [
        'airplane', 'automobile', 'bird', 'cat', 'deer',
        'dog', 'frog', 'horse', 'ship', 'truck'
    ]

    # Define the criterion and optimizer
    criterion = nn.CrossEntropyLoss()

    # Set the epsilon value for FGSM
    epsilon = 0.05

    # Test the model on adversarial examples and display the images
    test_adversarial_example(model, criterion, test_loader, epsilon, class_labels)


# main==============
def main():
    # set seeds for reproducibility
    torch.manual_seed(11171996)
    np.random.seed(11171996)
    # introduction_adverarial_Attack()
    
    # Load MNIST dataset with proper transformations
    train_loader, test_loader = get_CIFAR10_DataLoaders()
    
    # experiment3(train_loader, test_loader, 1000)
    
    # Define model
    model = get_Resnet_Model()
    model.fc = nn.Linear(2048, 10)

    optimizer = torch.optim.Adam(model.parameters())  
    # Fine tune model 
    for epoch in range(1, 10):
        train(model, train_loader, optimizer, epoch)
        # Test model
        print(f"Accuracy: {test(model, test_loader):.4f}")
    
    model = model.eval()
    experiment1(model, test_loader, cutoff=0.6)
    experiment2(model, train_loader, test_loader, 5)

    quit()

if __name__ == "__main__":
    main()