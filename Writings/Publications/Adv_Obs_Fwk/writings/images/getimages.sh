cp ../../../Code/Adversarial_Research/example/FGSM/*png .
cp ../../../Code/Adversarial_Research/example/*/* .
cp ../../../Code/Adversarial_Research/example/artifacts/ran_attack_vis/b*20* ran_best_20.png
cp ../../../Code/Adversarial_Research/example/artifacts/ran_attack_vis/e*20* ran_epoch_20.png
cp ../../../Code/Adversarial_Research/example/artifacts/ran_attack_vis/epoch_0.png ran_epoch_0.png

cp ../../../Code/Adversarial_Research/example/artifacts/5_attack_vis/b*20* 5_best_20.png
cp ../../../Code/Adversarial_Research/example/artifacts/5_attack_vis/best_1.png 5_best_1.png
cp ../../../Code/Adversarial_Research/example/artifacts/5_attack_vis/e*20* 5_epoch_20.png
cp ../../../Code/Adversarial_Research/example/artifacts/5_attack_vis/epoch_0.png 5_epoch_0.png