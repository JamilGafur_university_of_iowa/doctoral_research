\section{Current State of the Art}

\label{sec:CSOTA}

In the realm of eXplainable Artificial Intelligence (XAI), various libraries stand out, namely Quantus \cite{Hedstrom_Weber_Bareeva_Krakowczyk_Motzkus_Samek_Lapuschkin_Hohne_2023}, INNVestigate \cite{Alber_Lapuschkin_Seegerer_Hagele_Schutt_Montavon_Samek_Muller_Dahne_Kindermans}, and Captum \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan}. Each library is tailored for enhancing model interpretability, yet they exhibit distinct characteristics.

Quantus, designed to seamlessly integrate quantitative XAI into existing software frameworks, requires minimal code modification. It prioritizes a comprehensive array of metrics and is compatible with both PyTorch and TensorFlow frameworks. Its primary focus lies in supporting a wide range of evaluation metrics, ensuring a versatile tool for model understanding.

INNVestigate, in a similar vein, offers a library of XAI techniques but employs a ``wrapper'' programming style. Through the instantiation of an analyzer object tethered to a TensorFlow model, it treats the analyzer as the model during training, thereby enabling the analysis of model predictions using various XAI techniques post-training. This approach provides flexibility in incorporating XAI into the training pipeline.

Captum, a creation of Facebook AI, assumes a pivotal role with its specialized focus on PyTorch models and attribution methods. It stands out for its emphasis on transparency and interpretability. Captum facilitates an in-depth understanding of the nuanced impact of individual input features on model predictions. Its distinctive feature is its targeted application to PyTorch models, providing a valuable tool for those working within that framework.



\section{Techniques}

Each of these libraries focuses on developing XAI techniques, and while are under active development and have a wide range of techniques, they are not comprehensive. In Table \ref{tab:Techniques}, we have listed a few of the common XAI techniques that are implemented in each library. Each of these techniques is used for a different purpose, and while some are similar, they are not the same. In \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan} the authors descibe XAI techniques falling into six categories 

\begin{enumerate}
    \item Faithfulness: The degree to which the explanation accurately represents the model's decision-making process.
    \item Robustness: The degree to which the explanation remains consistent across different inputs.
    \item Localisation: The degree to which the explanation identifies the most relevant features.
    \item Complexity: The degree to which the explanation is easy to understand.
    \item Randomisation: The degree to which the explanation is independent of the model's output.
    \item Axiomatic Metrics: The degree to which the explanation satisfies a set of desirable properties.
\end{enumerate} 

\begin{table}[ht]
    \label{tab:Techniques}
    \centering
    \caption{XAI Methods Implemented in Each Library}
    \begin{tabular}{|p{3cm}|p{3cm}|p{3cm}|p{3cm}|}
        \hline
        \textbf{XAI Method} & \textbf{Quantus \cite{Hedstrom_Weber_Bareeva_Krakowczyk_Motzkus_Samek_Lapuschkin_Hohne_2023}} & \textbf{INNVestigate \cite{Alber_Lapuschkin_Seegerer_Hagele_Schutt_Montavon_Samek_Muller_Dahne_Kindermans}} & \textbf{Captum \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan_et}} \\
        \hline
        Shapley Values & & & \checkmark \\
        \hline
        LIME & & & \checkmark \\
        \hline
        Integrated Gradients & \checkmark & \checkmark & \checkmark \\
        \hline
        Feature Permutation & \checkmark & & \\
        \hline
        Layer-wise Relevance Propagation (LRP) & & \checkmark & \checkmark \\
        \hline
        Saliency Maps & & \checkmark & \checkmark \\
        \hline
        Deep Taylor Decomposition & & \checkmark & \\
        \hline
        PatternNet & & \checkmark & \\
        \hline
        DeepLIFT (Deep Learning Important FeaTures) & & & \checkmark \\
        \hline
        Occlusion Sensitivity & & & \checkmark \\
        \hline
    \end{tabular}
\end{table}


In this section, we discuss common techniques employed to enhance model interpretability. Adversarial attack algorithms, at the forefront of model interpretability, utilize either model gradients or input gradients to achieve their goals. One prevalent algorithm in this domain is the Fast Gradient Sign Method (FGSM) \cite{goodfellow2014explaining}. Another notable approach is Activation Mapping, which reveals insights into model behavior by mapping its activations. Additionally, the Shapley Additive Explanations (ShAP) method \cite{gramegna2021shap} contributes to interpretability by providing additive explanations of how input influences model output.

While these algorithms serve as potent tools for interpretability, like all eXplainable Artificial Intelligence (XAI) approaches, they are not without limitations and biases. Instead of introducing a new orthogonal approach, we have chosen to develop an interoperable framework that incorporates all these techniques. This integration allows us to leverage the collective power of these algorithms, surpassing their individual capabilities and providing a more comprehensive solution.

\subsection*{Fast Gradient Sign Method: Adversarial Attack}
The Fast Gradient Sign Method (FGSM) \cite{goodfellow2014explaining} is a well-known technique for generating adversarial examples. It strategically updates input data by leveraging the gradient of the loss function with respect to the input, inducing misclassification by the target model.

FGSM identifies influential features in the input data for the model's decision-making by analyzing the gradient's direction. It then updates the input data in the opposite direction of the gradient, with the magnitude determined by a small constant, $\epsilon$, ensuring imperceptibility to the human eye.

The FGSM equation for generating adversarial examples is defined as follows:
\begin{equation}
    \label{eq:FGSM}
    \mathbf{x}_{\text{adv}} = \mathbf{x} + \epsilon \cdot \text{sign}(\nabla_{\mathbf{x}} J(\theta, \mathbf{x}, y))
\end{equation}

Here, $\mathbf{x}$ is the input data, $\epsilon$ is the perturbation magnitude, $\nabla_{\mathbf{x}} J(\theta, \mathbf{x}, y)$ is the gradient of the loss function, and $\text{sign}(\cdot)$ is the sign function. FGSM is a one-shot attack, requiring only a single iteration for adversarial example generation \cite{goodfellow2014explaining}.

\subsection*{Adversarial Particle Swarm Optimization: Adversarial Attack}
The Adversarial Particle Swarm Optimization (APSO) algorithm utilizes Particle Swarm Optimization (PSO) \cite{kennedy1995particle} to search for perturbations in neural networks, aiming to maximize misclassification.

PSO is a heuristic optimization technique where a swarm of particles navigates a high-dimensional search space. Each particle represents a potential solution, updated iteratively based on its best-known position and the global best-known position of the swarm. PSO's algorithmic formulation is expressed as follows:
\begin{equation}
    \label{eq:PSO}
    \mathbf{v}_i^{t+1} = \omega \mathbf{v}_i^t + c_1r_1(\mathbf{p}_i^t - \mathbf{x}_i^t) + c_2r_2(\mathbf{p}_g^t - \mathbf{x}_i^t)
\end{equation}

In this equation, $\mathbf{v}_i^t$ is the velocity of particle $i$ at iteration $t$, $\omega$ is the inertia weight, $\mathbf{p}_i^t$ is the best-known position of particle $i$ at iteration $t$, $\mathbf{x}_i^t$ is the current position of particle $i$ at iteration $t$, and $c_1$, $c_2$, $r_1$, and $r_2$ are parameters.

APSO applies PSO to discover perturbations maximizing misclassification by optimizing the cost function. It iteratively explores the input space, updating positions and velocities to identify vulnerable regions in neural networks susceptible to adversarial attacks. Swarm algorithms like APSO have gained prominence for inducing misclassification in neural networks \cite{9716139, mosli2019they}.

\subsection*{Activation Mapping: Visualization \& Feature Attribution}
Activation Mapping facilitates the visualization of essential features used by machine learning models for prediction \cite{simonyan2013deep}. It analyzes the network's gradients for a given input, generating activation maps that assign higher scores to more significant features.

The Activation Mapping equation is expressed as:
\begin{equation}
    \label{eq:ActivationMapping}
    \mathbf{A}_i^c = \sum_k \frac{\partial y^c}{\partial \mathbf{a}_i^k}
\end{equation}

In this equation, $\mathbf{A}_i^c$ denotes the activation map for class $c$ at layer $i$, $y^c$ is the output score for class $c$, and $\mathbf{a}_i^k$ represents the activation of unit $k$ at layer $i$. Activation Mapping aids in comprehending the significance of input features for model predictions.

Activation maps highlight pixels crucial to the model's predictions, assisting in tasks like object localization. They contribute to model debugging, diagnosis, and provide insights into complex model decision-making processes. Our implementation is based on the research presented in Simonyan et al. \cite{simonyan2013deep}.

\subsection*{Shapley Additive Explanations (ShAP): Feature Attribution}
Shapley Additive Explanations (ShAP) \cite{lundberg2017unified, datta2016algorithmic} represents a feature attribution technique that quantitatively measures the importance of each feature in the input space for the model's output. Using Shapley values from game theory, ShAP estimates feature contributions by considering all possible subsets of features.

ShAP belongs to additive feature attribution methods \cite{lundberg2017unified}, explaining the model's output as a sum of real values assigned to each input feature. It possesses three key properties: local accuracy, missingness, and consistency \cite{datta2016algorithmic}. Model-agnostic, ShAP is applicable to any machine learning model, irrespective of architecture, size, or complexity, handling both classification and regression problems.

Our implementation of ShAP is based on the work of Lundberg et al. \cite{lundberg2017unified}.



% \subsection{Fast Gradient Sign Method: Adversarial Attack}
% The FGSM \cite{goodfellow2014explaining} is renowned for its effectiveness in generating adversarial examples. This method utilizes the gradient of the loss function with respect to the input to strategically update the input data, causing misclassification by the target model.

% By analyzing the gradient's direction, FGSM identifies the features of the input data that significantly impact the model's decision-making process. Subsequently, FGSM updates the input data by moving in the opposite direction of the gradient. The magnitude of these changes is determined by a small constant, $\epsilon$, which plays a crucial role in ensuring that the adversarial input remains imperceptible to the human eye. 

% The FGSM equation for generating adversarial examples is defined in equation \ref{eq:FSGM}
% \begin{equation}
%     \label{eq:FSGM}
%     \mathbf{x}_{adv} = \mathbf{x} + \epsilon \cdot \text{sign}(\nabla_{\mathbf{x}} J(\theta, \mathbf{x}, y))
% \end{equation}

% where $\mathbf{x}$ is the input data, $\epsilon$ is the magnitude of the perturbation, $\nabla_{\mathbf{x}} J(\theta, \mathbf{x}, y)$ is the gradient of the loss function with respect to the input, and $\text{sign}(\cdot)$ is the sign function. The FGSM algorithm is a one-shot attack, meaning that it only requires one iteration to generate adversarial examples. Our implementation of the FGSM algorithm is based on the research presented in Goodfellow et al.~\cite{goodfellow2014explaining}.

% \subsection{Adversarial Particle Swarm Optimization: Adversarial Attack}
% The APSO algorithm leverages the Particle Swarm Optimization (PSO) method \cite{kennedy1995particle} to effectively search for perturbations in neural networks, aiming to maximize misclassification.

% PSO is a heuristic optimization technique where a swarm of particles navigates through a high-dimensional search space. Each particle represents a potential solution and has a position and velocity, updated iteratively based on its own best-known position and the global best-known position of the swarm. The velocity indicates the particle's movement direction and magnitude in the search space. The PSO algorithm is defined by Equation \ref{eq:PSO1}.

% \begin{equation}
% \label{eq:PSO1}
% \mathbf{v}{i}^{t+1} = \omega \mathbf{v}{i}^{t} + c_{1}r_{1}(\mathbf{p}{i}^{t} - \mathbf{x}{i}^{t}) + c_{2}r_{2}(\mathbf{p}{g}^{t} - \mathbf{x}{i}^{t})
% \end{equation}

% In Equation \ref{eq:PSO1}, $\mathbf{v}{i}^{t}$ is the velocity of particle $i$ at iteration $t$, $\omega$ is the inertia weight, $\mathbf{p}{i}^{t}$ is the best-known position of particle $i$ at iteration $t$, $\mathbf{x}{i}^{t}$ is the current position of particle $i$ at iteration $t$, $c{1}$ and $c_{2}$ are the cognitive and social parameters, respectively, and $r_{1}$ and $r_{2}$ are random numbers between 0 and 1.

% Within the context of APSO, particles collectively aim to discover perturbations that maximize misclassification by optimizing the cost function. APSO explores the input space iteratively by updating positions and velocities, identifying regions where neural networks are most susceptible to adversarial attacks.

% Heuristic swarm algorithms like APSO have gained prominence in attacking neural networks and inducing misclassification \cite{9716139, mosli2019they}. These algorithms generate perturbed input images capable of deceiving machine learning models. Iterative approaches such as APSO offer adaptability across various models, exploiting decision-making vulnerabilities and resulting in higher success rates for adversarial examples \cite{mosli2019they}.

% APSO has showcased remarkable efficacy in generating adversarial examples that successfully deceive machine learning models while maintaining the natural appearance of the original input. By capitalizing on the inherent biases and decision boundaries of the model, APSO adeptly generates adversarial examples that bypass the model's defenses. The iterative nature of APSO enables continuous improvement and refinement of the perturbations, maximizing its effectiveness in generating potent adversarial examples. This flexibility and adaptability position APSO as a valuable tool for exploring the robustness and vulnerability of machine learning models when confronted with adversarial attacks.

% Our implementation of APSO is based on the work of Mosli et al. \cite{mosli2019they}. We have designed this algorithm to accept any cost function defined by the developer, allowing for its use in a wide range of applications.

% \subsection{Activation Mapping: Visualization \& Feature Attribution}
% Activation mapping enables the visualization of essential features used by machine learning models for prediction \cite{simonyan2013deep}. It analyzes the network's gradients for a given input and generates activation maps, which assign higher scores to more significant features as defined in Equation \ref{eq:AM}.

% \begin{equation}
% \label{eq:AM}
% \mathbf{A}{i}^{c} = \sum{k} \frac{\partial y^{c}}{\partial \mathbf{a}_{i}^{k}}
% \end{equation}

% Equation \ref{eq:AM} represents the activation map equation, where $\mathbf{A}{i}^{c}$ denotes the activation map for class $c$ at layer $i$, $y^{c}$ is the output score for class $c$, and $\mathbf{a}{i}^{k}$ represents the activation of unit $k$ at layer $i$.

% Activation mapping enhances the robustness of machine learning models by identifying critical features for classification and understanding how they influence the generation of adversarial examples. It is commonly used in machine learning and computer vision to comprehend the significance of input features for model predictions.

% In the context of image analysis, activation maps highlight pixels crucial to the model's predictions. Visualizing these maps helps identify regions in an image that strongly influence the model's output. This information is valuable for tasks such as object localization, where precise determination of an object's location within an image is crucial.  These maps assist in model debugging, diagnosis, and offer insights into the decision-making process of complex models, ultimately aiding the development of more accurate and transparent ML systems.

% Our implementation of activation mapping is based on the research presented in Simonyan et al. \cite{simonyan2013deep}.


% \subsection{Shapley Additive Explanations (ShAP): Feature Attribution}
% ShAP \cite{lundberg2017unified}, \cite{datta2016algorithmic} represents a cutting-edge feature attribution technique that offers valuable insights into the behavior of complex machine learning models. This approach quantitatively measures the importance of each feature in the input space for the model's output. By leveraging the concept of Shapley values from game theory, ShAP estimates the contribution of each input feature to the final prediction by considering all possible subsets of features. It allocates the contribution of each feature among the subsets it participates in, based on its marginal contribution to the prediction.

% ShAP belongs to the class of additive feature attribution methods \cite{lundberg2017unified}, which explain the model's output as a sum of real values assigned to each input feature. Such methods are widely employed for interpreting individual predictions of deep learning models. What sets ShAP apart is its unique solution characterized by three desirable properties: local accuracy, missingness, and consistency \cite{datta2016algorithmic}. Moreover, the method is model-agnostic, making it applicable to any machine learning model, irrespective of its architecture, size, or complexity. ShAP is versatile and can handle both classification and regression problems. It is also capable of handling a wide range of input types, including images, text, and tabular data.

% We have implemented the ShAP algorithm based on the work of Lundbert~et.~al \cite{lundberg2017unified} into our framework.






% Currently, many libraries exist that offer XAI techniques such as Quantus \cite{Hedstrom_Weber_Bareeva_Krakowczyk_Motzkus_Samek_Lapuschkin_Hohne_2023}, INNVestigate \cite{Alber_Lapuschkin_Seegerer_Hagele_Schutt_Montavon_Samek_Muller_Dahne_Kindermans} and Captum \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan_et}.  Each of these libraries are specifically designed for model understanding, while the Adversrial Observation paper is build for newer algorithm development via inter-operable methods and include AA techniques.

% Quantus \cite{Hedstrom_Weber_Bareeva_Krakowczyk_Motzkus_Samek_Lapuschkin_Hohne_2023} was designed with the goal of developing quantitative XAI into existing software frameworks with a minimal change in code. The main goal of this library was focused on supporting a ``breadth of metrics''. This library was designed to work on both the Pytorch and Tensorflow framework. 

% INNVestigate \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan_et} similarly provides a library with XAI techniuqes, but uses a ``wrapper'' like format. This works by instantiating and analyzer object that fits tethered to a tensorflow model, then during training the analyzer is treated as the model and passes the training data to the model. Once the training is compleate the analyzer object can be used to analyze the models predictions using similar XAI techniques.

% Captum \cite{Kokhlikyan_Miglani_Martin_Wang_Alsallakh_Reynolds_Melnikov_Kliushkina_Araya_Yan_et}, developed by Facebook AI, assumes a pivotal role with its specialized focus on PyTorch models and attribution methods. Tailored for transparency and interpretability, Captum aids in understanding the nuanced impact of individual input features on model predictions.