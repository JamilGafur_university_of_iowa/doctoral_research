# The Application of Heuristic Swarm Algorithms for Neural Network Interpretability

## Introduction
Neural Networks have witnessed a surge in their utilization within both academic research and industrial applications. Their ability to model complex relationships and make accurate predictions has made them a staple in various fields. However, as their applications extend into critical domains such as healthcare, finance, and autonomous systems, the need for understanding and interpreting their decision-making processes becomes paramount.

In contexts where model interpretability and security are essential, Neural Networks often pose challenges. While there are established methods for interpreting machine learning models, they fall into two broad categories: model-agnostic and model-specific approaches. Model-agnostic methods, such as LIME (Local Interpretable Model-agnostic Explanations) and SHAP (SHapley Additive exPlanations), offer a generic means of explaining model predictions. However, their accuracy diminishes as model complexity and dimensionality increase. On the other hand, model-specific methods provide interpretable insights tailored to the particular neural network architecture but require training on data explicitly designed for interpretability.

## Problem
The central challenge in interpreting neural networks lies in the absence of a straightforward and universally applicable method for extracting meaningful insights from these complex models. Model-agnostic methods, such as LIME and SHAP, are limited by their reduced explainability as the dimensionality of neural networks grows. Model-specific methods, while capable of offering tailored insights, often lack robustness and depend on access to specially curated interpretable datasets.

## Solution
This project proposes a novel approach to enhance the interpretability of neural networks by harnessing the power of heuristic swarm algorithms. While heuristic swarm algorithms are not traditionally associated with producing human-readable results, they can serve as a valuable tool for generating spatio-temporal explanations. These explanations can aid in achieving both local interpretability (understanding model predictions for individual inputs) and potentially global interpretability (comprehending the neural network's overall behavior within the context of epigenomic principles).

## Methodology
The methodology for achieving enhanced interpretability in neural networks is as follows:

- [x] Design a comprehensive framework capable of performing Saliency mapping and Heuristic Swarm Attacks. This framework will serve as the backbone for extracting interpretable insights from neural networks.

- [x] Train a set of Neural Networks using enhancer data. Enhancers, which play a crucial role in gene regulation, offer a suitable context for training neural networks to achieve biologically relevant interpretability.

- [x] Utilize the developed framework to generate a saliency map for a given input. Saliency maps will provide a visual representation of the most influential features for specific model predictions, aiding in local interpretability.

- [x] Leverage the framework to generate a heuristic swarm attack for a given input. This approach will explore the neural network's decision space, shedding light on the factors influencing its predictions.

- [ ] Conduct a study where we input randomly generated sparse noise as the initialization of the swarm and have it optimize to the high prediction of enhancers; confirm if this is implicitly learning the rules of enhancers following epigenomics (global interpretability).


- [ ] Explore the possibility of achieving local interpretability by leveraging the framework by inputting the same enhancer with small noise added in and optimizing for an enhancer that is similar to the original input and maximizing the activations.


## Ideas/Papers:
1. This can be used on enhancer data, but I would like to see how it is done on ImangeNet data
    * I think it would be optimal to load in 3 neural networks that are trained on ImageNet data and then run the saliency mapping and heuristic swarm attacks on them and compare the output for Local interpretability

2. For Scipy Conference: submit a paper for the framework 






